const gulp = require('gulp');
const sass = require('gulp-sass');
const bs = require('browser-sync');

//компилируем sass
gulp.task('sass', function () {
   return gulp.src(['src/scss/**/*.sass'])
      .pipe(sass())
      .pipe(gulp.dest('./dist/css'))
      .pipe(bs.stream());
})

//компилируем библиотеки на sass
gulp.task('libs',function(){
      return gulp.src(['src/libs/**/**/*.scss'])
            .pipe(sass())
            .pipe(gulp.dest('./dist/libs'))
            .pipe(bs.stream());
})

//копируем .html файлы
gulp.task('html',function(){
	return gulp.src(['src/*.html'])
		.pipe(gulp.dest('./dist/'))
		.pipe(bs.stream());
})
//копируем картинки
gulp.task('img',function(){
	return gulp.src(['./src/img/*'])
		.pipe(gulp.dest('./dist/img/'))
		.pipe(bs.stream());
})
gulp.task('icons',function(){
	return gulp.src(['./src/icons/*'])
		.pipe(gulp.dest('./dist/icons/'))
		.pipe(bs.stream());
})
//копируем фонты
gulp.task('fonts', function() {
	return gulp.src(['./src/fonts/*'])
                    .pipe(gulp.dest('dist/fonts/'))
                    .pipe(bs.stream());
});
//копируем js
gulp.task('js', function() {
	return gulp.src(['./src/js/*.js'])
                    .pipe(gulp.dest('dist/js/'))
                    .pipe(bs.stream());
});

gulp.task('serve', ['sass','libs','fonts','img','icons','html','js'], function () {
   bs.init({
      server: './dist'
   });
	gulp.watch(['src/scss/*.sass'], ['sass']);
      gulp.watch(['src/fonts/*.[otf|ttf]'], ['fonts']);
      gulp.watch(['src/img/*'], ['img']);
      gulp.watch(['src/icons/*'], ['icons']);
      gulp.watch(['src/*.html'],['html']);
      gulp.watch(['src/js/*.js'],['js']);
	gulp.watch(['src/libs/**/**/*.scss'],['libs']);
})

gulp.task('default', ['serve']);