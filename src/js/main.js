/* $('.fade').slick({
   infinite: true,
   speed: 500,
   fade: true,
   cssEase: 'linear',
   dotsClass: 'custom_paging',
   prevArrow: $('.left-arrow'),
   nextArrow: $('.right-arrow'),
   customPaging: function (slider, i) {
      //FYI just have a look at the object to find available information
      //press f12 to access the console in most browsers
      //you could also debug or look in the source
      console.log(slider);
      return  (i + 1) + '/' + slider.slideCount;
  }
 }); */


 $(function() {
   $('.fade').on('init', function(event, slick) {
      var current = slick.currentSlide + 1;
      var total = slick.slideCount;
      console.log(current)
      if (current > 9){
         $('.fade-current').text(slick.currentSlide + 1)
      }else{
         $('.fade-current').text('0' + (slick.currentSlide + 1))
      }
      if (total > 9){
         $('.fade-total').text(slick.slideCount)
      }
      else
      {
         $('.fade-total').text('0' + slick.slideCount)
      }
     /* $('.slider-current').text(slick.currentSlide + 1);
     $('.slider-number').text(slick.slideCount); */
   })
   .slick({
   infinite: true,
   speed: 500,
   fade: true,
   cssEase: 'linear',
   dotsClass: 'custom_paging',
   prevArrow: $('.fade-left-arrow'),
   nextArrow: $('.fade-right-arrow'),
   })
   .on('beforeChange', function(event, slick, currentSlide, nextSlide) {
      var current = slick.currentSlide + 1;
      if (current > 9){
         $('.slider-current').text(slick.currentSlide + 1)
      }else{
         $('.slider-current').text('0' + (slick.currentSlide + 1))
      }
   });
 });
 $(function() {
   $('.gallery-row__left-slider').on('init', function(event, slick) {
      var current = slick.currentSlide + 1;
      var total = slick.slideCount;
      console.log(current)
      if (current > 9){
         $('.gallery-current').text(slick.currentSlide + 1)
      }else{
         $('.gallery-current').text('0' + (slick.currentSlide + 1))
      }
      if (total > 9){
         $('.gallery-total').text(slick.slideCount)
      }
      else
      {
         $('.gallery-total').text('0' + slick.slideCount)
      }
     /* $('.slider-current').text(slick.currentSlide + 1);
     $('.slider-number').text(slick.slideCount); */
   })
   .slick({
   infinite: true,
   speed: 500,
   fade: true,
   cssEase: 'linear',
   dotsClass: 'custom_paging',
   prevArrow: $('.left-arrow'),
   nextArrow: $('.right-arrow'),
   })
   .on('beforeChange', function(event, slick, currentSlide, nextSlide) {
      var current = slick.currentSlide + 1;
      if (current > 9){
         $('.slider-current').text(slick.currentSlide + 1)
      }else{
         $('.slider-current').text('0' + (slick.currentSlide + 1))
      }
   });
 });


 $('.partners-slider').slick({
   infinite: true,
   arrows: false,
   slidesToShow: 6,
   slidesToScroll: 1,
   speed: 500
 });